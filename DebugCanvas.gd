extends Node2D

var draw_list = []
func _draw():
	for d in draw_list:
		draw_line(d[0], d[1], Color(255, 0, 0))
	draw_list = []

func add_line(line):
	draw_list.append(line)
