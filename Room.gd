class_name Room

var shape: Rect2;
var doors: Array;

var position = null setget ,get_position
var size = null setget ,get_size
var end = null setget ,get_end

func _init(p: Vector2, s: Vector2):
	shape = Rect2(p, s)

func get_position() -> Vector2:
	return shape.position

func get_size() -> Vector2:
	return shape.size

func get_end() -> Vector2:
	return shape.end

func add_door(point: Vector2) -> bool:
	doors.append(point)
	return true

func randi_range(mini: int, maxi: int) -> int:
	var diff = maxi - mini
	if diff == 0:
		return mini
	else:
		return randi() % diff + mini

func get_random_edge_point(avoid_corners = true) -> Vector2:
	var side = randi() % 4
	var door_site
	match side:
		# Top
		0:
			door_site = Vector2(
				rand_range(shape.position.x, shape.end.x),
				shape.position.y
				)
		# Right
		1:
			door_site = Vector2(
				shape.end.x - 1,
				rand_range(shape.position.y, shape.end.y)
				)
		# Bottom
		2:
			door_site = Vector2(
				rand_range(shape.position.x, shape.end.x),
				shape.end.y - 1
				)
		# Left
		3:
			door_site = Vector2(
				shape.position.x,
				rand_range(shape.position.y, shape.end.y)
				)
		_:
			# ...this should never happen but it's hear to make the type system happy
			door_site = shape.position
	
	var corners = [
		shape.position,
		Vector2(shape.end.x, shape.position.y),
		shape.end,
		Vector2(shape.position.x, shape.end.y)
		]
	
	if avoid_corners && door_site in corners:
		print('CORNER FOUND')

	return door_site

func get_doors() -> Array:
	return doors
