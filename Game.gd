extends Node2D

var Room = preload('Room.gd')

enum Tiles { Grass, Brick, Door, Black }

var map_size = Vector2(60, 40)
var room_size_min = Vector2(10, 10)
var room_size_max = Vector2(15, 15)

var should_update = false
var field_of_view = []

func _ready():
	randomize()

	# Build the map and place the player in a random room.
	var rooms = build_map(map_size.x, map_size.y)
	spawn_player(rooms)
	should_update = true

func _physics_process(_delta):
	if should_update:
		for t in field_of_view:
			$FieldOfView.set_cell(t.x, t.y, 0)

		field_of_view = update_field_of_view($Player.position, 5)

		for t in field_of_view:
			$FieldOfView.set_cell(t.x, t.y, -1)

		# Draw debug graphics.
		$DebugCanvas.update()
		should_update = false

func _input(event):
	if event.is_pressed():
		var cell_size = $FloorMap.cell_size
		var direction = Vector2(0, 0)

		if Input.is_action_pressed('ui_up'):
			direction.y = -1
		if Input.is_action_pressed('ui_down'):
			direction.y = 1
		if Input.is_action_pressed('ui_left'):
			direction.x = -1
		if Input.is_action_pressed('ui_right'):
			direction.x = 1

		if direction.length():
			var mapCoords = $FloorMap.world_to_map($Player.position)
			var canMove = can_move_to(mapCoords + direction)

			if canMove:
				$Player.position.x += direction.x * cell_size.x
				$Player.position.y += direction.y * cell_size.y

				should_update = true

func update_field_of_view(origin_position: Vector2, radius: int) -> Array:
	origin_position += $FloorMap.cell_size / 2
	var space_state = get_world_2d().direct_space_state
	var x_offset_range = range(-radius, radius + 1)
	var y_offset_range = range(-radius, radius + 1)
	var fov = []

	for x_offset in x_offset_range:
		for y_offset in y_offset_range:
			var offset_cell = Vector2(x_offset, y_offset)
			var target_corner_position = origin_position + $FloorMap.map_to_world(offset_cell)
			target_corner_position += get_corner_offset(origin_position, target_corner_position)
			target_corner_position -= $FloorMap.cell_size / 2

			var target_cell = $FloorMap.world_to_map(origin_position) + offset_cell
			var result = space_state.intersect_ray(origin_position, target_corner_position)

			if result.empty():
				fov.append(target_cell)
			else:
				$DebugCanvas.add_line([origin_position, target_corner_position])
				var collision_point_diff = target_corner_position - result.position

				# Display walls that we can see (but nothing beyond them)
				var collided_tile_type = $FloorMap.get_cell(result.metadata.x, result.metadata.y)
				var blocks_vision = collided_tile_type == Tiles.Brick || collided_tile_type == Tiles.Black
				var is_same = abs(collision_point_diff.x) <= 0 && abs(collision_point_diff.y) <= 0
				if blocks_vision && is_same:
					fov.append(target_cell)

	return fov

func get_corner_offset(origin: Vector2, target: Vector2) -> Vector2:
	var result = Vector2(0, 0)
	if origin.x > target.x:
		result.x += $FloorMap.cell_size.x
	if origin.y > target.y:
		result.y += $FloorMap.cell_size.y
	return result

func can_move_to(coords):
	var tile = $FloorMap.get_cell(coords.x, coords.y) 
	if tile < 0 || tile == Tiles.Brick:
		return false
	else:
		return true

func spawn_player(rooms):
	var spawn_room = rooms[randi() % rooms.size()]
	var spawn_map_threshold = spawn_room.end - spawn_room.position
	var spawn_map_position = Vector2(
		randi() % spawn_map_threshold.x as int,
		randi() % spawn_map_threshold.y as int
		) + spawn_room.position

	$Player.position = $FloorMap.map_to_world(spawn_map_position)

func build_map(map_width, map_height):
	reset_map(map_width, map_height)

	var free_spaces = [Rect2(Vector2(2, 2), Vector2(map_width - 4, map_height - 4))]
	var rooms = []

	var safety = 0
	while free_spaces.size() > 0:
		var results = build_rooms(free_spaces)

		rooms.append(results[0])
		free_spaces = results[1]

		safety += 1
		if (safety >= 50):
			print('BAIL!')
			break;

	for r in rooms:
		draw_room(r)

	build_corridors()

	fill_map(map_width, map_height)

	return rooms

# Fill in all empty cells with brick.
func fill_map(map_width, map_height):
	for x in map_width:
		for y in map_height:
			if $FloorMap.get_cell(x, y) == -1:
				$FloorMap.set_cell(x, y, Tiles.Brick)

# Reset the tile map to get ready to generate a new map.
func reset_map(map_width, map_height):
	for x in map_width:
		for y in map_height:
			$FieldOfView.set_cell(x, y, 0)
			$FloorMap.set_cell(x, y, -1)

func draw_room(room):
	for x in range(room.position.x, room.end.x):
		for y in range(room.position.y, room.end.y):
			if x == room.position.x || x ==  room.end.x - 1 || y == room.position.y || y == room.end.y - 1:
				$FloorMap.set_cell(x, y, Tiles.Brick)
			else:
				$FloorMap.set_cell(x, y, Tiles.Grass)
	
	for door in room.doors:
		$FloorMap.set_cell(door.x, door.y, Tiles.Door)

func build_rooms(free_spaces):
	var build_site = free_spaces[randi() % free_spaces.size()]

	var new_room = build_single_room(build_site)
	var new_free_spaces = build_free_spaces(free_spaces, new_room.shape)
	
	return [new_room, new_free_spaces]

func build_single_room(outer_rect):
	var sizeVariance = Vector2(
		min(outer_rect.size.x, room_size_max.x),
		min(outer_rect.size.y, room_size_max.y)
		) - room_size_min

	var dimensions = Vector2(room_size_min)
	if sizeVariance.x > 0:
		dimensions.x += (randi() % int(sizeVariance.x))
	if sizeVariance.y > 0:
		dimensions.y += (randi() % int(sizeVariance.y))

	var positionVariance = outer_rect.size - dimensions + Vector2(1, 1)
	var position = Vector2(outer_rect.position.x, outer_rect.position.y)
	if positionVariance.x > 0:
		position.x += (randi() % int(positionVariance.x))
	if positionVariance.y > 0:
		position.y += (randi() % int(positionVariance.y))

	# TODO: Add doors to rooms before returning
	var room = Room.new(position, dimensions)
	room.add_door(room.get_random_edge_point())

	return room

func build_free_spaces(free_regions, region_to_remove):
	var add_queue = []
	var remove_queue = []

	for region in free_regions:
		if region.intersects(region_to_remove):
			remove_queue.append(region)

			var left = region_to_remove.position.x - region.position.x - 1
			var right = region.end.x - region_to_remove.end.x - 1
			var above = region_to_remove.position.y - region.position.y - 1
			var below = region.end.y - region_to_remove.end.y - 1

			if left >= room_size_min.x:
				add_queue.append(
					Rect2(
						region.position,
						Vector2(left, region.size.y)
						)
					)
			if right >= room_size_min.x:
				add_queue.append(
					Rect2(
						Vector2(region_to_remove.end.x + 1, region.position.y),
						Vector2(right, region.size.y)
						)
					)
			if above >= room_size_min.y:
				add_queue.append(
					Rect2(
						region.position,
						Vector2(region.size.x, above)
						)
					)
			if below >= room_size_min.y:
				add_queue.append(
					Rect2(
						Vector2(region.position.x, region_to_remove.end.y + 1),
						Vector2(region.size.x, below)
						)
					)

	for to_remove in remove_queue:
		free_regions.erase(to_remove)
	
	for to_add in add_queue:
		free_regions.append(to_add)

	return free_regions

func build_corridors():
	var graph = AStar2D.new()
	var doors = []
	var id = 0

	for x in map_size.x:
		for y in map_size.y:
			var tile = $FloorMap.get_cell(x, y)

			if tile != Tiles.Brick:
				graph.add_point(id, Vector2(x, y))

				# Connect surrounding tiles.
				var left_tile = $FloorMap.get_cell(x - 1, y) 
				var left_tile_id = graph.get_closest_point(Vector2(x - 1, y))
				var left_tile_coords = graph.get_point_position(left_tile_id)

				var top_tile = $FloorMap.get_cell(x, y - 1) 
				var top_tile_id = graph.get_closest_point(Vector2(x, y - 1))
				var top_tile_coords = graph.get_point_position(top_tile_id)

				# Since AStar returns the _closet point_ in the graph the coords that
				# are provided, we'll want to make sure that the coords match _exactly_
				# to avoid connecting points that shouldn't be connected.
				var leftMatch = x > 0 && left_tile_coords.x == x - 1 && left_tile_coords.y == y
				var topMatch = y > 0 && top_tile_coords.x == x && top_tile_coords.y == y - 1

				if leftMatch && left_tile != Tiles.Brick:
					graph.connect_points(id, left_tile_id)
				if topMatch && top_tile != Tiles.Brick:
					graph.connect_points(id, top_tile_id)

			if tile == Tiles.Door:
				doors.push_back(id)

			id += 1
	
	for di in range(doors.size() - 1):
		var path = graph.get_point_path(doors[di], doors[di + 1])
		for p in path:
			$FloorMap.set_cell(p.x, p.y, Tiles.Grass)
